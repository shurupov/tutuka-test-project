# Comparison Project

This is a test home-project for Tutuka

After the server is started you will see a page similar to this Balsamiq mockup:

![](mockup-transaction-compare.png)

An example of csv-files to compare are here:

* [clientmarkofffile20140114.csv](clientmarkofffile20140114.csv)
* [tutukamarkofffile20140114.csv](tutukamarkofffile20140114.csv)

To get more detailed task click [here](welcome-to-tutuka-product-development-team.pdf).

## Command to init/start
`docker-compose up` for linux

`docker compose up` for windows

This command starts the application from scratch. You just need to have docker-compose installed for linux or docker installed for windows.
For the first execution this will build docker images and start them. It can take 10 minutes. For the next execution it just starts them.

## Command to init/start project in dev mode
`docker-compose --file docker-compose-dev.yml up` for linux

`docker compose --file docker-compose-dev.yml up` for windows

This command will start jre docker image and nginx docker image tuned to work with built backend and frontend. To start them you need to build backend and frontend first. 


## Build backend for dev mode
`mvn clean package`

This command should be executed in a `backend` folder. Maven should be installed.

## Build frontend for dev mode
`npm install`

`npm run build`

This command should be executed in a `frontend` folder. Nodejs and npm should be installed.


