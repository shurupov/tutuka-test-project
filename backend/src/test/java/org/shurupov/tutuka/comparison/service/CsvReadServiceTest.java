package org.shurupov.tutuka.comparison.service;

import org.junit.jupiter.api.Test;
import org.shurupov.tutuka.comparison.domain.Transaction;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CsvReadServiceTest {

    private CsvReadService csvReadService = new CsvReadService();

    private static String CSV_FILE_CONTENT =
        "ProfileName,TransactionDate,TransactionAmount,TransactionNarrative,TransactionDescription,TransactionID,TransactionType,WalletReference\n" +
        "Card Campaign,2014-01-11 22:27:44,-20000,*MOLEPS ATM25             MOLEPOLOLE    BW,DEDUCT,0584011808649511,1,P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5,\n" +
        "Card Campaign,2014-01-11 22:39:11,-10000,*MOGODITSHANE2            MOGODITHSANE  BW,DEDUCT,0584011815513406,1,P_NzI1MjA1NjZfMTM3ODczODI3Mi4wNzY5,";

    @Test
    void read() throws IOException {
        MultipartFile multipartFile = new MockMultipartFile("name", CSV_FILE_CONTENT.getBytes(StandardCharsets.UTF_8));

        List<Transaction> transactions = csvReadService.read(multipartFile);

        assertEquals(2, transactions.size());

        assertEquals("Card Campaign", transactions.get(0).getProfileName());
        assertEquals("2014-01-11 22:27:44", transactions.get(0).getTransactionDate());
        assertEquals(-20000L, transactions.get(0).getTransactionAmount());
        assertEquals("*MOLEPS ATM25             MOLEPOLOLE    BW", transactions.get(0).getTransactionNarrative());
        assertEquals("DEDUCT", transactions.get(0).getTransactionDescription());
        assertEquals(new BigInteger("0584011808649511"), transactions.get(0).getTransactionId());
        assertEquals(1, transactions.get(0).getTransactionType());
        assertEquals("P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5", transactions.get(0).getWalletReference());

        assertEquals("Card Campaign", transactions.get(1).getProfileName());
        assertEquals("2014-01-11 22:39:11", transactions.get(1).getTransactionDate());
        assertEquals(-10000L, transactions.get(1).getTransactionAmount());
        assertEquals("*MOGODITSHANE2            MOGODITHSANE  BW", transactions.get(1).getTransactionNarrative());
        assertEquals("DEDUCT", transactions.get(1).getTransactionDescription());
        assertEquals(new BigInteger("0584011815513406"), transactions.get(1).getTransactionId());
        assertEquals(1, transactions.get(1).getTransactionType());
        assertEquals("P_NzI1MjA1NjZfMTM3ODczODI3Mi4wNzY5", transactions.get(1).getWalletReference());
    }
}