package org.shurupov.tutuka.comparison.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.shurupov.tutuka.comparison.domain.ComparisonReport;
import org.shurupov.tutuka.comparison.domain.Transaction;
import org.shurupov.tutuka.comparison.domain.TransactionReport;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CompareServiceTest {

    private CsvReadService csvReadService;
    private CompareService compareService;

    @BeforeEach
    public void init() {
        csvReadService = mock(CsvReadService.class);
        compareService = new CompareService(csvReadService);
    }

    @Test
    void process() throws IOException {
        MultipartFile file1 = new MockMultipartFile("file1.csv", "file1.csv", null, (byte[]) null);
        MultipartFile file2 = new MockMultipartFile("file2.csv", "file2.csv", null, (byte[]) null);

        when(csvReadService.read(eq(file1))).thenReturn(transactionList1());
        when(csvReadService.read(eq(file2))).thenReturn(transactionList2());

        ComparisonReport report = compareService.process(List.of(file1, file2));

        assertEquals(2, report.getSummaries().size());

        assertEquals("file1.csv", report.getSummaries().get(0).getFileName());
        assertEquals(3, report.getSummaries().get(0).getTotalRecords());
        assertEquals(1, report.getSummaries().get(0).getMatchedRecords());
        assertEquals(2, report.getSummaries().get(0).getUnmatchedRecords());

        assertEquals("file2.csv", report.getSummaries().get(1).getFileName());
        assertEquals(3, report.getSummaries().get(1).getTotalRecords());
        assertEquals(1, report.getSummaries().get(1).getMatchedRecords());
        assertEquals(2, report.getSummaries().get(1).getUnmatchedRecords());

        assertEquals(3, report.getUnmatchedReports().size());
    }

    @Test
    void compare() {
        Map<String, Queue<Transaction>> stringQueueMap = new LinkedHashMap<>();
        stringQueueMap.put("file1.csv", new LinkedList<>(transactionList1()));
        stringQueueMap.put("file2.csv", new LinkedList<>(transactionList2()));

        ComparisonReport report = compareService.compare(stringQueueMap);

        assertEquals("file1.csv", report.getSummaries().get(0).getFileName());
        assertEquals(3, report.getSummaries().get(0).getTotalRecords());
        assertEquals(1, report.getSummaries().get(0).getMatchedRecords());
        assertEquals(2, report.getSummaries().get(0).getUnmatchedRecords());

        assertEquals("file2.csv", report.getSummaries().get(1).getFileName());
        assertEquals(3, report.getSummaries().get(1).getTotalRecords());
        assertEquals(1, report.getSummaries().get(1).getMatchedRecords());
        assertEquals(2, report.getSummaries().get(1).getUnmatchedRecords());

        assertEquals(3, report.getUnmatchedReports().size());
    }

    @Test
    void countMatched() {
        Map<String, Queue<Transaction>> stringQueueMap = new LinkedHashMap<>();
        stringQueueMap.put("file1.csv", new LinkedList<>(transactionList1()));
        stringQueueMap.put("file2.csv", new LinkedList<>(transactionList2()));

        List<Map.Entry<String, Queue<Transaction>>> entries = new ArrayList<>(stringQueueMap.entrySet());

        Map<String, Integer> matched = compareService.countMatched(entries.get(0), entries.get(1));

        assertEquals(2, matched.size());

        assertEquals(1, matched.get("file1.csv"));
        assertEquals(1, matched.get("file2.csv"));
    }

    @Test
    void findUnmatched() {

        Map<String, Queue<Transaction>> stringQueueMap = new LinkedHashMap<>();
        stringQueueMap.put("file1.csv", new LinkedList<>(transactionList1()));
        stringQueueMap.put("file2.csv", new LinkedList<>(transactionList2()));

        List<Map.Entry<String, Queue<Transaction>>> entries = new ArrayList<>(stringQueueMap.entrySet());

        compareService.countMatched(entries.get(0), entries.get(1));

        List<TransactionReport> reports = compareService.findUnmatched(entries.get(0), entries.get(1));

        assertEquals(3, reports.size());

        assertEquals(2, reports.get(0).getTransactionAmount().size());
        assertEquals(2, reports.get(0).getWalletReference().size());
        assertEquals(2, reports.get(1).getTransactionAmount().size());
        assertEquals(2, reports.get(2).getTransactionAmount().size());
    }

    @Test
    void buildComparison1() {
        Map<String, Long> fieldValues1 = compareService.buildComparison(buildTransactionMap(), Transaction::getTransactionAmount);

        assertEquals(2, fieldValues1.size());
        assertEquals(100L, fieldValues1.get("file1.csv"));
        assertEquals(101L, fieldValues1.get("file2.csv"));

        Map<String, String> fieldValues2 = compareService.buildComparison(buildTransactionMap(), Transaction::getWalletReference);

        assertEquals(1, fieldValues2.size());
        assertEquals("ref1", fieldValues2.get("all"));
    }

    @Test
    void BuildComparison2() {
        TransactionReport report = compareService.buildComparison(buildTransactionMap());

        assertEquals(1, report.getWalletReference().size());
        assertEquals("ref1", report.getWalletReference().get("all"));
        assertEquals(1, report.getTransactionType().size());
        assertEquals(0, report.getTransactionType().get("all"));
        assertEquals(2, report.getTransactionDate().size());
        assertEquals("today", report.getTransactionDate().get("file1.csv"));
        assertEquals("yesterday", report.getTransactionDate().get("file2.csv"));
        assertEquals(1, report.getTransactionDescription().size());
        assertEquals("to me", report.getTransactionDescription().get("all"));
        assertEquals(2, report.getTransactionAmount().size());
        assertEquals(100L, report.getTransactionAmount().get("file1.csv"));
        assertEquals(101L, report.getTransactionAmount().get("file2.csv"));
    }

    private Map<String, Transaction> buildTransactionMap() {
        Map<String, Transaction> transactionMap = new LinkedHashMap<>();
        transactionMap.put("file1.csv", Transaction.builder()
            .walletReference("ref1")
            .transactionType(0)
            .transactionDate("today")
            .transactionDescription("to me")
            .transactionAmount(100L)
            .transactionNarrative("")
            .profileName("")
            .transactionId(new BigInteger("123"))
            .build());
        transactionMap.put("file2.csv", Transaction.builder()
            .walletReference("ref1")
            .transactionType(0)
            .transactionDate("yesterday")
            .transactionDescription("to me")
            .transactionAmount(101L)
            .transactionNarrative("")
            .profileName("")
            .transactionId(new BigInteger("123"))
            .build());
        return transactionMap;
    }

    private List<Transaction> transactionList1() {
        return List.of(
            transaction("123", "123", 1000L),
            transaction("124", "124", -1000L),
            transaction("125", "125", 500L)
        );
    }

    private List<Transaction> transactionList2() {
        return List.of(
            transaction("123", "123", 1000L),
            transaction("124", "125", -1500L),
            transaction("126", "126", 600L)
        );
    }

    private Transaction transaction(String id, String postfix, Long amount) {
        return Transaction.builder()
            .walletReference("ref" + postfix)
            .transactionType(0)
            .transactionDate("today")
            .transactionDescription("to me")
            .transactionAmount(amount)
            .transactionNarrative("narrative1")
            .profileName("profile1")
            .transactionId(new BigInteger(id))
            .build();
    }
}