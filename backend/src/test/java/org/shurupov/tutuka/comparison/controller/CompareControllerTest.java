package org.shurupov.tutuka.comparison.controller;

import org.junit.jupiter.api.Test;
import org.shurupov.tutuka.comparison.domain.ComparisonReport;
import org.shurupov.tutuka.comparison.domain.CsvSummary;
import org.shurupov.tutuka.comparison.domain.Transaction;
import org.shurupov.tutuka.comparison.domain.TransactionReport;
import org.shurupov.tutuka.comparison.service.CompareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CompareController.class)
class CompareControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompareService compareService;

    @Test
    void compare() throws Exception {

        ComparisonReport comparisonReport = new ComparisonReport(
            List.of(
                new CsvSummary("file1.csv", 20, 19),
                new CsvSummary("file2.csv", 20, 19)
            ),
            List.of(
                report1()
            )
        );

        when(compareService.process(any())).thenReturn(comparisonReport);

        mockMvc.perform(
            multipart("/api/compare")
                .file("files", "12345".getBytes(StandardCharsets.UTF_8))
                .file("files", "67890".getBytes(StandardCharsets.UTF_8))
        ).andExpect(status().isOk())
            .andExpect(jsonPath("$.summaries", hasSize(2)))
            .andExpect(jsonPath("$.summaries[0].fileName", is("file1.csv")))
            .andExpect(jsonPath("$.summaries[0].totalRecords", is(20)))
            .andExpect(jsonPath("$.summaries[0].matchedRecords", is(19)))
            .andExpect(jsonPath("$.summaries[0].unmatchedRecords", is(1)))
            .andExpect(jsonPath("$.summaries[1].fileName", is("file2.csv")))
            .andExpect(jsonPath("$.summaries[1].totalRecords", is(20)))
            .andExpect(jsonPath("$.summaries[1].matchedRecords", is(19)))
            .andExpect(jsonPath("$.summaries[1].unmatchedRecords", is(1)))
            .andExpect(jsonPath("$.unmatchedReports", hasSize(1)))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionDate", aMapWithSize(1)))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionDate.all", is("today")))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionAmount", aMapWithSize(2)))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionAmount['file1.csv']", is(1000)))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionAmount['file2.csv']", is(998)))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionId.all", is(123)))
            .andExpect(jsonPath("$.unmatchedReports[0].transactionType.all", is(0)))
            .andExpect(jsonPath("$.unmatchedReports[0].walletReference.all", is("Reference")))
        ;
    }

    private TransactionReport report1() {
        return TransactionReport.builder()
            .transactionDate(Map.of("all", "today"))
            .transactionAmount(Map.of("file1.csv", 1000L, "file2.csv", 998L))
            .transactionId(Map.of("all", new BigInteger("123")))
            .transactionDescription(Map.of("all", "Description"))
            .transactionType(Map.of("all", 0))
            .walletReference(Map.of("all", "Reference"))
            .build();
    }
}