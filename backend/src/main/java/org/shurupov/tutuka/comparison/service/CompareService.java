package org.shurupov.tutuka.comparison.service;

import lombok.RequiredArgsConstructor;
import org.shurupov.tutuka.comparison.domain.ComparisonReport;
import org.shurupov.tutuka.comparison.domain.CsvSummary;
import org.shurupov.tutuka.comparison.domain.Transaction;
import org.shurupov.tutuka.comparison.domain.TransactionReport;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CompareService {

    private static final String CSV_EXTENSION = ".csv";

    private final CsvReadService csvReadService;

    public ComparisonReport process(List<MultipartFile> files) throws IOException {

        Map<String, Queue<Transaction>> transactionQueues = new HashMap<>();

        for (MultipartFile file : files) {
            if (!Objects.requireNonNull(file.getOriginalFilename()).toLowerCase().endsWith(CSV_EXTENSION)) {
                continue;
            }

            String name = file.getOriginalFilename();
            transactionQueues.put(name, parseTransactions(file));
        }

        return compare(transactionQueues);
    }

    protected ComparisonReport compare(Map<String, Queue<Transaction>> transactionsQueues) {

        Map<String, Integer> totals = transactionsQueues.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size()));

        Map.Entry<String, Queue<Transaction>> main = transactionsQueues.entrySet().stream().findFirst().get();
        String mainFile = main.getKey();
        transactionsQueues.remove(mainFile);
        Map.Entry<String, Queue<Transaction>> second = transactionsQueues.entrySet().stream().findFirst().get();

        Map<String, Integer> matched = countMatched(main, second);

        String secondFile = second.getKey();
        CsvSummary mainSummary   = new CsvSummary(mainFile, totals.get(mainFile), matched.get(mainFile));
        CsvSummary secondSummary = new CsvSummary(secondFile, totals.get(secondFile), matched.get(secondFile));

        return new ComparisonReport(
            List.of(mainSummary, secondSummary),
            findUnmatched(main, second)
        );
    }

    protected Map<String, Integer> countMatched(Map.Entry<String, Queue<Transaction>> main, Map.Entry<String, Queue<Transaction>> second) {
        String mainFile = main.getKey();

        Map<String, Integer> matched = new HashMap<>();
        matched.put(mainFile, 0);
        matched.put(second.getKey(), 0);

        List<Transaction> mainUnmatched = new LinkedList<>();

        Queue<Transaction> mainQueue = main.getValue();

        while (!mainQueue.isEmpty()) {
            Transaction mainTransaction = mainQueue.poll();

            Optional<Transaction> found = second.getValue().stream()
                .filter(t -> t.equals(mainTransaction))
                .findFirst();
            if (found.isPresent()) {
                matched.put(mainFile, matched.get(mainFile) + 1);
                matched.put(second.getKey(), matched.get(second.getKey()) + 1);
                second.getValue().remove(found.get());
            } else {
                mainUnmatched.add(mainTransaction);
            }
        }
        mainQueue.addAll(mainUnmatched);

        return matched;
    }

    protected List<TransactionReport> findUnmatched(Map.Entry<String, Queue<Transaction>> main, Map.Entry<String, Queue<Transaction>> second) {
        List<TransactionReport> reports = new ArrayList<>();
        while (!main.getValue().isEmpty()) {
            Transaction transaction = main.getValue().poll();
            Optional<Transaction> found = second.getValue().stream()
                .filter(t ->
                    t.getTransactionId().equals(transaction.getTransactionId())
                    && t.getTransactionDescription().equals(transaction.getTransactionDescription())
                )
                .findFirst();
            if (found.isEmpty()) {
                found = second.getValue().stream()
                    .filter(t ->
                        t.getTransactionId().equals(transaction.getTransactionId())
                    )
                    .findFirst();
            }
            Map<String, Transaction> reportRaw;
            if (found.isPresent()) {
                reportRaw = Map.of(
                    main.getKey(), transaction,
                    second.getKey(), found.get()
                );
                second.getValue().remove(found.get());
            } else {
                reportRaw = Map.of(
                    main.getKey(), transaction,
                    second.getKey(), Transaction.builder().build()
                );
            }
            reports.add(buildComparison(reportRaw));
        }
        while (!second.getValue().isEmpty()) {
            Map<String, Transaction> reportRaw = Map.of(
                main.getKey(), Transaction.builder().build(),
                second.getKey(), second.getValue().poll()
            );
            reports.add(buildComparison(reportRaw));
        }
        return reports;
    }

    protected <T> Map<String, T> buildComparison(Map<String, Transaction> source, Function<Transaction, T> getMethod) {
        List<T> values = source.values().stream()
            .map(getMethod)
            .collect(Collectors.toList());
        if (Objects.equals(values.get(0), values.get(1))) {
            return Map.of("all", values.get(0));
        }
        Map<String, T> comparison = new LinkedHashMap<>();
        for (Map.Entry<String, Transaction> entry : source.entrySet()) {
            comparison.put(entry.getKey(), getMethod.apply(entry.getValue()));
        }
        return comparison;
    }

    protected TransactionReport buildComparison(Map<String, Transaction> source) {
        if (source == null) {
            return null;
        }

        return TransactionReport.builder()
            .transactionDate(buildComparison(source, Transaction::getTransactionDate))
            .transactionAmount(buildComparison(source, Transaction::getTransactionAmount))
            .transactionId(buildComparison(source, Transaction::getTransactionId))
            .transactionDescription(buildComparison(source, Transaction::getTransactionDescription))
            .transactionType(buildComparison(source, Transaction::getTransactionType))
            .walletReference(buildComparison(source, Transaction::getWalletReference))
            .build();
    }

    private Queue<Transaction> parseTransactions(MultipartFile multipartFile) throws IOException {
        return new LinkedList<>(csvReadService.read(multipartFile));
    }
}
