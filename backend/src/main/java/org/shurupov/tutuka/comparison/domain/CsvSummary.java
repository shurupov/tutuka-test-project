package org.shurupov.tutuka.comparison.domain;

import lombok.Data;

@Data
public class CsvSummary {

    public CsvSummary(String fileName, int totalRecords, int matchedRecords) {
        this.fileName = fileName;
        this.totalRecords = totalRecords;
        this.matchedRecords = matchedRecords;
        this.unmatchedRecords = totalRecords - matchedRecords;
    }

    private final String fileName;
    private final int totalRecords;
    private final int matchedRecords;
    private final int unmatchedRecords;
}
