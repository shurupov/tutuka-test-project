package org.shurupov.tutuka.comparison.controller;

import lombok.RequiredArgsConstructor;
import org.shurupov.tutuka.comparison.domain.ComparisonReport;
import org.shurupov.tutuka.comparison.service.CompareService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;

@RestController
@RequiredArgsConstructor
public class CompareController {

    private final CompareService compareService;

    @PostMapping("/api/compare")
    public ComparisonReport compare(@RequestParam("files") MultipartFile[] files) throws IOException {
        return compareService.process(Arrays.asList(files));
    }
}
