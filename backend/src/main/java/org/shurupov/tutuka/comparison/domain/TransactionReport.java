package org.shurupov.tutuka.comparison.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.util.Map;

@Data
@Builder
public class TransactionReport {
    private Map<String, String> transactionDate;
    private Map<String, Long> transactionAmount;
    private Map<String, BigInteger> transactionId;
    private Map<String, String> transactionDescription;
    private Map<String, Integer> transactionType;
    private Map<String, String> walletReference;
}
