package org.shurupov.tutuka.comparison.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Configuration
public class UploadConfig {

    private static final long MAX_UPLOADED_FILES_TOTAL_SIZE = 100_000_000_000L;

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(MAX_UPLOADED_FILES_TOTAL_SIZE);
        return multipartResolver;
    }
}
