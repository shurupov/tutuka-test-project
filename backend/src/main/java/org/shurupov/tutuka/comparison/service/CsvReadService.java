package org.shurupov.tutuka.comparison.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.shurupov.tutuka.comparison.domain.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CsvReadService {

    private static final CSVFormat TRANSACTIONS_CSV_FORMAT = CSVFormat.Builder.create(CSVFormat.DEFAULT)
        .setHeader(
            Arrays.stream(Transaction.class.getFields())
                .map(Field::getName)
                .toArray(String[]::new)
        )
        .setSkipHeaderRecord(false)
        .build();


    private Transaction map(CSVRecord record) {
        return Transaction.builder()
            .profileName(record.get("ProfileName"))
            .transactionDate(record.get("TransactionDate"))
            .transactionAmount(Long.parseLong(record.get("TransactionAmount")))
            .transactionNarrative(record.get("TransactionNarrative"))
            .transactionDescription(record.get("TransactionDescription"))
            .transactionId(new BigInteger(record.get("TransactionID")))
            .transactionType(Integer.parseInt(record.get("TransactionType")))
            .walletReference(record.get("WalletReference"))
            .build();
    }

    public List<Transaction> read(MultipartFile multipartFile) throws IOException {
        return CSVParser
            .parse(multipartFile.getInputStream(), StandardCharsets.UTF_8, TRANSACTIONS_CSV_FORMAT)
            .stream()
            .map(this::map)
            .collect(Collectors.toList());
    }
}
