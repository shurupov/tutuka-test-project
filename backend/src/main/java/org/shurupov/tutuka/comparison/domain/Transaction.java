package org.shurupov.tutuka.comparison.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;

@Data
@Builder
public class Transaction {
    private String profileName;
    private String transactionDate;
    private Long transactionAmount;
    private String transactionNarrative;
    private String transactionDescription;
    private BigInteger transactionId;
    private Integer transactionType;
    private String walletReference;
}
