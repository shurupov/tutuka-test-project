package org.shurupov.tutuka.comparison.domain;

import lombok.Data;

import java.util.List;

@Data
public class ComparisonReport {
    private final List<CsvSummary> summaries;
    private final List<TransactionReport> unmatchedReports;
}
