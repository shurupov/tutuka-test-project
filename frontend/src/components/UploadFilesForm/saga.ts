import {call, takeEvery, select, put} from "redux-saga/effects";
import {sagaActionTypes} from "store/sagaActionType";
import {AnyAction} from "redux";
import {AppState} from "store/store";
import {filesSlice} from "components/UploadFilesForm/slice";
import {Result, resultSlice} from "components/ComparisonResults/slice";

export const filesToUploadSelector = (state: AppState) => state.files;

export const uploadFilesAction = (): AnyAction => {
    return {
        type: sagaActionTypes.FILES_UPLOAD,
    };
};

export function* workerUploadFiles(): any {

    const url = '/api/compare';
    const files = yield select(filesToUploadSelector);

    const body = new FormData();
    for (const file of files) {
        body.append("files", file);
    }

    const requestSettings: RequestInit = {
        method: 'POST',
        mode: "cors",
        body,
    };
    const response: Response = yield call(fetch, process.env.service + url, requestSettings);
    const jsonResponse: Result = yield call([response, 'json']);
    yield put(resultSlice.actions.show(jsonResponse));
}

export function* watchUploadFiles(): any {
    yield takeEvery(sagaActionTypes.FILES_UPLOAD, workerUploadFiles);
}