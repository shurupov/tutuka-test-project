import React from "react";
import {
    Button,
    Upload,
    Space, Card,
} from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import {RcFile, UploadFile} from "antd/lib/upload/interface";

export interface UploadFilesFormProps {
    fileList: Array<RcFile>;
    add: (file: RcFile) => boolean;
    remove: (file: UploadFile) => boolean;
    upload: () => void;
}

export const UploadFilesForm = (props: UploadFilesFormProps): React.ReactElement => (
    <Card title="Specify files to compare" bordered={true} style={{ marginBottom: 20 }}>
        <Space direction="vertical" style={{ width: "100%", marginBottom: 20 }}>
            <Upload listType="picture"
                    action="/api/compare"
                    beforeUpload={props.add}
                    fileList={props.fileList}
                    onRemove={props.remove}
                    multiple={true}
                    type="drag"
            >
                <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                </p>
                <p className="ant-upload-text">Click or drag csv-file to this area to upload</p>
                <p className="ant-upload-hint">
                    Supports for a single or bulk upload.
                </p>
            </Upload>
        </Space>

        <Button type="primary" onClick={props.upload}>Upload and compare</Button>
    </Card>
);