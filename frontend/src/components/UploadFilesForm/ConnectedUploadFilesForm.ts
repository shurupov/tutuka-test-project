import {connect} from "react-redux";
import {UploadFilesForm, UploadFilesFormProps} from "components/UploadFilesForm/UploadFilesForm";
import {Dispatch} from "redux";
import {filesSlice} from "components/UploadFilesForm/slice";
import {RcFile} from "antd/lib/upload/interface";
import {AppState} from "store/store";
import {uploadFilesAction} from "components/UploadFilesForm/saga";

const mapStateToProps = (storeState: AppState) => {
    return {
        fileList: storeState.files as Array<RcFile>,
    }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        add: (file: RcFile) => {
            dispatch(filesSlice.actions.add(file));
            return false;
        },
        remove: (file: RcFile) => {
            dispatch(filesSlice.actions.remove(file));
            return false;
        },
        upload: () => {
            dispatch(uploadFilesAction());
        }
    }
};

export const ConnectedUploadFilesForm = connect(mapStateToProps, mapDispatchToProps)(UploadFilesForm);