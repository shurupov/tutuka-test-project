import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RcFile, UploadFile} from "antd/lib/upload/interface";

export const filesSlice = createSlice({
    name: "files",
    initialState: [] as Array<RcFile>,
    reducers: {
        add: (state: Array<RcFile>, action: PayloadAction<RcFile>) => {
            if (action.payload.name.toLowerCase().endsWith(".csv")) {
                return [
                    ...state,
                    action.payload
                ]
            } else {
                return state;
            }

        },
        remove: (state: Array<RcFile>, action: PayloadAction<UploadFile>) => {
            const newFileList:Array<any> = [];
            for (let i = 0; i < state.length; i++) {
                if (state[i] != action.payload) {
                    newFileList.push(state[i]);
                }
            }
            return newFileList;
        },
    }
})