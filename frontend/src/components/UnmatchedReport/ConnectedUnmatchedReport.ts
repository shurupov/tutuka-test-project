import {AppState} from "store/store";
import {connect} from "react-redux";
import {UnmatchedReport} from "components/UnmatchedReport/UnmatchedReport";

const mapStateToProps = (storeState: AppState) => {
    return storeState.result;
};

export const ConnectedUnmatchedReport = connect(mapStateToProps)(UnmatchedReport);