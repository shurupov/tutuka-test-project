import {ResultsState} from "components/ComparisonResults/slice";
import React from "react";
import {Card, List, Table, Tooltip} from "antd";

export const UnmatchedReport = (props: ResultsState): React.ReactElement => {
    if (!props.showUnmatchedReport) {
        return null;
    }

    const renderMethod = (map: any) => {
        if (map.hasOwnProperty('all')) {
            return map['all'];
        }
        const items = [];
        for (const prop in map) {
            items.push(
                <Tooltip title={prop} key={prop} placement="right">
                    <List.Item>
                        {
                            map[prop] != null && map[prop] != ''
                                ? map[prop]
                                : "<empty>"
                        }
                    </List.Item>
                </Tooltip>
            );
        }
        return <List
            bordered
            dataSource={items}
            renderItem={item => item}
        />;
    };

    const columns = [
        {
            title: 'Date',
            dataIndex: 'transactionDate',
            key: 'transactionDate',
            render: renderMethod,
        },
        {
            title: 'Amount',
            dataIndex: 'transactionAmount',
            key: 'transactionAmount',
            render: renderMethod,
        },
        {
            title: 'Transaction Id',
            dataIndex: 'transactionId',
            key: 'transactionId',
            render: renderMethod,
        },
        {
            title: 'Description',
            dataIndex: 'transactionDescription',
            key: 'transactionDescription',
            render: renderMethod,
        },
        {
            title: 'Type',
            dataIndex: 'transactionType',
            key: 'transactionType',
            render: renderMethod,
        },
        {
            title: 'Wallet Reference',
            dataIndex: 'walletReference',
            key: 'walletReference',
            render: renderMethod,
        },
    ];

    const data = props.unmatchedReports;

    return <Card title="Unmatched report" bordered={true} style={{ marginBottom: 20 }}>
        <Table columns={columns} dataSource={data} />
    </Card>;
}