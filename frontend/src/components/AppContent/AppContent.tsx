import React from "react";
import {ConnectedUploadFilesForm} from "components/UploadFilesForm/ConnectedUploadFilesForm";
import {ConnectedComparisonResult} from "components/ComparisonResults/ConnectedComparisonResult";
import {ConnectedUnmatchedReport} from "components/UnmatchedReport/ConnectedUnmatchedReport";

export const AppContent = () => (
    <div className="site-layout-content">
        <ConnectedUploadFilesForm />
        <ConnectedComparisonResult />
        <ConnectedUnmatchedReport />
    </div>
);