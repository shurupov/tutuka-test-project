import {Card, Col, Row, Descriptions, Button} from "antd";
import React from "react";
import {ResultsState, Summary} from "components/ComparisonResults/slice";

export interface ComparisonResultProps extends ResultsState{
    showReport: () => void;
}

export const ComparisonResult = (props: ComparisonResultProps): React.ReactElement => {
    if (!props.visible) {
        return null;
    }

    return <Card title="Comparison results" bordered={true} style={{ marginBottom: 20 }}
                 actions={[
                     <Button key="showReport" type="primary" onClick={props.showReport}>Unmatched Report</Button>
                 ]}
    >
        <Row>
            {
                props.summaries.map((summary: Summary) => (
                    <Col key={summary.fileName} span={12} style={{ padding: 10 }}>
                        <Card title={summary.fileName} bordered={true}>
                            <Card.Grid style={{ width: '100%', padding: 0 }}>
                                <Descriptions
                                    bordered
                                    column={1}
                                >
                                    <Descriptions.Item label="Total Records">{summary.totalRecords}</Descriptions.Item>
                                    <Descriptions.Item label="Matched Records">{summary.matchedRecords}</Descriptions.Item>
                                    <Descriptions.Item label="Unmatched Records">{summary.unmatchedRecords}</Descriptions.Item>
                                </Descriptions>
                            </Card.Grid>
                        </Card>
                    </Col>
                ))
            }
        </Row>
    </Card>
}