import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export interface Summary {
    fileName: string;
    totalRecords: number;
    matchedRecords: number;
    unmatchedRecords: number;
}

export interface Result {
    summaries: Array<Summary>;
    unmatchedReports: Array<any>;
}

export interface ResultsState extends Result {
    visible: boolean;
    showUnmatchedReport: boolean;
}

export const resultSlice = createSlice({
    name: "result",
    initialState: {
        visible: false,
        showUnmatchedReport: false,
        summaries: [],
    } as ResultsState,
    reducers: {
        show: (state: ResultsState, action: PayloadAction<Result>) => {
            return {
                ...action.payload,
                visible: true,
                showUnmatchedReport: false
            }
        },
        showReport: (state: ResultsState) => {
            return {
                ...state,
                showUnmatchedReport: !state.showUnmatchedReport
            }
        }
    }
});