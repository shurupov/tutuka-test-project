import {connect} from "react-redux";
import {AppState} from "store/store";
import {ComparisonResult} from "components/ComparisonResults/ComparisonResult";
import {Dispatch} from "redux";
import {resultSlice} from "components/ComparisonResults/slice";

const mapStateToProps = (storeState: AppState) => {
    return storeState.result;
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        showReport: () => {
            dispatch(resultSlice.actions.showReport());
        }
    }
};

export const ConnectedComparisonResult = connect(mapStateToProps, mapDispatchToProps)(ComparisonResult);