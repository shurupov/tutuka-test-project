import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import "antd/dist/antd.css";
import "./App.css";
import {store} from "store/store";
import {Provider} from "react-redux";
import {AppContent} from "components/AppContent/AppContent";

const { Header, Content, Footer } = Layout;

export const App = () => (
    <Provider store={store}>
        <Layout className="layout">
            <Header>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                    <Menu.Item key={1}>Transaction Comparisons</Menu.Item>
                </Menu>
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>Comparisons</Breadcrumb.Item>
                </Breadcrumb>
                <AppContent />
            </Content>
            <Footer style={{ textAlign: 'center' }}>Tutuka Comparison</Footer>
        </Layout>
    </Provider>
);