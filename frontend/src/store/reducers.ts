import {combineReducers} from "redux";
import {filesSlice} from "components/UploadFilesForm/slice";
import {resultSlice} from "components/ComparisonResults/slice";

export const createRootReducer = () => combineReducers({
    files: filesSlice.reducer,
    result: resultSlice.reducer,
});