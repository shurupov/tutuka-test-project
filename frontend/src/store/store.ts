import {applyMiddleware, compose, createStore, PreloadedState, Store} from "redux";
import {createRootReducer} from "store/reducers";
import createSagaMiddleware from "redux-saga";
import {composeWithDevTools} from "redux-devtools-extension";
import {RcFile} from "antd/lib/upload/interface";
import {watchUploadFiles} from "components/UploadFilesForm/saga";
import {Result, ResultsState} from "components/ComparisonResults/slice";

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(preloadedState: PreloadedState<any>): Store {
    const store = createStore(
        createRootReducer(), // root reducer with router state
        preloadedState,
        compose(
            composeWithDevTools(
                applyMiddleware(
                    sagaMiddleware
                )
            ),
        ),
    )

    return store;
}

export const store: Store = configureStore({});

export interface AppState {
    files: Array<RcFile>;
    result: ResultsState
}

sagaMiddleware.run(watchUploadFiles);